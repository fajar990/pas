#!/bin/bash

NUM_CPU_CORES=$(nproc --all)

cpulimit -e "wesboh" -l $((30 * $NUM_CPU_CORES))